package dev.android.jamie;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import dev.android.space.R;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        String str = parent.getItemAtPosition(pos).toString();
        if (str.equals("Rookie")) {
            Log.d("game", "Started Advanced game");
        } else if (str.equals("Expert")) {
            Log.d("game", "Started Advanced game");
        } else if (str.equals("Master")) {
            Log.d("game", "Started Advanced game");
        } else if (str.equals("Orange")) {
            scenario.bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.spaceship3_hdpi);
        } else if (str.equals("Yellow")) {
            scenario.bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.spaceship2_hdpi);
        } else if (str.equals("Purple")) {
            scenario.bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.right_side_hdpi);
        }
    }
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }
    OnScoreListener onScoreListener = new OnScoreListener() {
        @Override
        public void onScore(final int score) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.setText("Score: " + score);
                }
            });
        }
    };
    private void togglePausePlay() {
        if (scenario.paused) {
            // play
            //  getSupportActionBar().hide();
            Toast.makeText(MainActivity.this, "Play", Toast.LENGTH_SHORT).show();
        } else {
            // pause
            //    getSupportActionBar().show();
            Toast.makeText(MainActivity.this, "Pause", Toast.LENGTH_SHORT).show();
        }
        scenario.paused = !scenario.paused;
    }

    public TextView textView;
    public LinearLayout mainLayout;
    String[] spinnerValue2 = {"Ship", "Purple", "Orange", "Yellow"};
    //String[] spinnerValue = {"Difficulty", "Rookie", "Advanced", "Expert", "Master"};

    Scenario scenario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int w = size.x;
        int h = size.y;
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.right_side_hdpi);

        mainLayout = new LinearLayout(this);
        mainLayout.setOrientation(LinearLayout.VERTICAL);

        LinearLayout menuLayout = new LinearLayout(this);
        menuLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));

        textView = new TextView(this);
        textView.setVisibility(View.VISIBLE);
        String str = "Score: 0";
        textView.setText(str);
        menuLayout.addView(textView);

        Button button = new Button(this);
        button.setText("Pause");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                togglePausePlay();
            }
        });
        menuLayout.addView(button);
        Spinner spinner = new Spinner(this);
        spinner.setOnItemSelectedListener(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, spinnerValue2);
        spinner.setAdapter(adapter);
        menuLayout.addView(spinner);
        mainLayout.addView(menuLayout);
        Scenario scenario = new Scenario(this, bitmap, w, h, this, onScoreListener);
        mainLayout.addView(scenario);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setContentView(mainLayout);
    }
}