package dev.android.jamie;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.util.Random;

import dev.android.space.R;

interface OnScoreListener {
    void onScore(int score);
}

public class Scenario extends SurfaceView implements SurfaceHolder.Callback {

    Bitmap bitmap;
    private Simulation thread;
    private int x = 25, y = 25;
    int width, height;
    int NBRSTEPS; // number of discrete positions in the x-dimension; must be uneven
    String heroName;
    int screenW;
    int screenH;
    int[] x1; // x-coordinates for falling objects
    int[] y1; // y-coordinates for falling objects
    Random random = new Random();
    int badGuyWidth; // width of each bad guy
    int badGuyHeight; // height of ditto
    float dY; //vertical speed
    Bitmap falling, heroBitmap, jamieright, falling2, falling3;
    int heroXCoord;
    int heroYCoord;
    int xsteps;
    int score;
    boolean gameOver; // default value is false
    boolean toastDisplayed;
    boolean paused = false;
    MainActivity activity;
    OnScoreListener onScoreListener;
    public Scenario(Context context, Bitmap bmp, int w, int h, MainActivity a, OnScoreListener onScoreListener) {
        super(context);
        width = w;
        height = h;
        bitmap = bmp;
        dY = 6.0f;
        this.activity = a;
        a.scenario = this;
        NBRSTEPS = 7; // must be uneven
        heroName = "Purple";
        //this.onScoreListener = onScoreListener;
        x = (int) w / 2;
        y = (int) (h *0.8);
        x1 = new int[NBRSTEPS];
        y1 = new int[NBRSTEPS];
        int resourceIdFalling = 0;
        for (int i = 0; i < y1.length; i++) {
            y1[i] = -random.nextInt(1000); // place items randomly in vertical direction
        }
        int resourceIdFalling2;
        int resourceIdFalling3;
        resourceIdFalling3 = R.drawable.object3_hdpi;
        resourceIdFalling = R.drawable.object2_hdpi;
        resourceIdFalling2 = R.drawable.object1_hdpi;
        falling = BitmapFactory.decodeResource(getResources(), resourceIdFalling); //load a falling image
        falling2 = BitmapFactory.decodeResource(getResources(), resourceIdFalling2); //load a falling image
        falling3 = BitmapFactory.decodeResource(getResources(), resourceIdFalling3); //load a falling image
        heroBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.right_side_hdpi); //load a hero image
        jamieright = BitmapFactory.decodeResource(getResources(), R.drawable.right_side_hdpi); //load a hero image
        badGuyWidth = falling.getWidth();
        badGuyHeight = falling.getHeight();
        int maxOffset = (NBRSTEPS - 1) / 2;
        xsteps = w / NBRSTEPS;

        Display display = a.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenW = size.x;
        //int h = size.y;
        this.onScoreListener = onScoreListener;
        for (int i = 0; i < x1.length; i++) {
            int origin = screenW / 2 + xsteps * (i - maxOffset);
            x1[i] = origin - (badGuyWidth / 2);
        }

        thread = new Simulation(getHolder(), this);
        getHolder().addCallback(this);
        setFocusable(true);
    }

    // method called when the screen opens
    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        screenW = w;
        screenH = h;
        xsteps = w / NBRSTEPS;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Bitmap backgroundImage = BitmapFactory.decodeResource(getResources(), R.drawable.app_bg1_xxxhdpi);
        canvas.drawBitmap(backgroundImage, new Rect(0, 0, backgroundImage.getWidth(), backgroundImage.getHeight()), new RectF(0, 0, canvas.getWidth(), canvas.getHeight()), null);
        canvas.drawBitmap(bitmap, x - (bitmap.getWidth() / 2), y - (bitmap.getHeight() / 2), null);

        int heroHeight = heroBitmap.getHeight();
        int heroWidth = heroBitmap.getWidth();


        // compute locations of falling objects
        for (int i2 = 0; i2 < y1.length; i2++) {
            if (!paused) {
                y1[i2] += (int) dY;
            }
            // if falling object hits bottom of screen
            if (y1[i2] > (screenH - badGuyHeight) && !gameOver) {
                dY = 0;
                gameOver = true;
                // paused = true;
                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        final Toast toast = Toast.makeText(activity.scenario.getContext(), "GAME OVER!\nScore: " + score, Toast.LENGTH_SHORT);
                                        toast.show();
                                        Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                toast.cancel();
                                                toastDisplayed = true;
                                            }
                                        }, 3000);
                                        //Vibrator v = (Vibrator) context.getSystemService(context.VIBRATOR_SERVICE);
                                        // Vibrate for 3000 milliseconds
                                        //v.vibrate(3000);
                                    }

                                });
            }
            // if the hero catches an alien
             if (java.lang.Math.abs(x1[i2] - heroXCoord) * 2  < (badGuyWidth + heroWidth) && java.lang.Math.abs(y1[i2] - heroYCoord) * 2  < (badGuyHeight + heroHeight)) {
                y1[i2] = -random.nextInt(1000); // reset to new vertical position
                score += 1;
                 onScoreListener.onScore(score);
            }
        }
        canvas.save(); //Save the position of the canvas. Is this really necessary?
        for (int i = 0; i < y1.length; i++) {
            if (i % 3 == 1) {
                canvas.drawBitmap(falling2, x1[i], y1[i], null); //Draw the falling on the canvas.
            } else if (i % 3 == 2) {
                canvas.drawBitmap(falling, x1[i], y1[i], null); //Draw the falling on the canvas.
            } else if (i % 3 == 0) {
                canvas.drawBitmap(falling3, x1[i], y1[i], null); //Draw the falling on the canvas.
            }
        }
        canvas.restore(); // Must?
        invalidate(); // Must?
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if(gameOver) {

            gameOver = false;
            x1 = new int[NBRSTEPS];
            y1 = new int[NBRSTEPS];
            int maxOffset = (NBRSTEPS - 1) / 2;

            for (int i = 0; i < y1.length; i++) {
                y1[i] = -random.nextInt(1000); // place items randomly in vertical direction
            }
            for (int i = 0; i < x1.length; i++) {
                int origin = screenW / 2 + xsteps * (i - maxOffset);
                x1[i] = origin - (badGuyWidth / 2);
            }
            score = 0;
            dY = 6.0f;
        }

        x = (int) event.getX();
        y = (int) event.getY();

        if (x < 25)
            x = 25;
        if (x > width)
            x = width;
        if (y < 25)
            y = 25;
        if (y > height)
            y = height;

        heroXCoord = x;
        heroYCoord = y;
        return true;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // TODO Auto-generated method stub
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        thread.startrun(true);
        thread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        thread.startrun(false);
//        thread.stop();
    }
}