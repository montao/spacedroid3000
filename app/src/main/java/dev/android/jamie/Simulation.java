package dev.android.jamie;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class Simulation extends Thread {

    private SurfaceHolder holder;
    private Scenario scenario;
    private boolean aBoolean = false;

    public Simulation(SurfaceHolder holder, Scenario scenario) {
        this.holder = holder;
        this.scenario = scenario;
    }

    public void startrun(boolean run) {
        aBoolean = run;
    }

    @Override
    public void run() {
        super.run();
        Canvas canvas;
        while (aBoolean) {
            canvas = null;
            try {
                canvas = holder.lockCanvas(null);
                synchronized (holder) {
                    if (canvas != null) {
                        scenario.onDraw(canvas);
                    }
                }
            } finally {
                if (canvas != null) {
                    holder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }
}
